const dotenv = require("dotenv");

dotenv.config({
  path: `${__dirname}/.env`
});

export const config = {
  title: process.env.TITLE
};
